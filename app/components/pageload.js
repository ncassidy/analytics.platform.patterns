(function () {

    var _helper = {
        getDefaultData: function (data) {
            var defaultData = {
                path: data.path,
                context: data.context,
                events: 'event21'
            };

            return defaultData;
        }
    };


    $(window).on('pageloaded', function (event, data) {
        switch (data.path) {
            // Specific Page
            case '/example-path/2':
                    var trackingData = _helper.getDefaultData(data);
                    trackingData.events = 'event21,event67';
                    window.analytics.services.vendor.executePageloadTracking(trackingData);
                    break;

            // All Pages
            default:
                var trackingData = _helper.getDefaultData(data);
                window.analytics.services.vendor.executePageloadTracking(trackingData);
                break;
        }
    });

}());




// Trigger Example
var data = {
    path: '/',
    context: {
        pageName: 'Vail Resort:Home',
        currency: 'USD'
    },
};
$(window).trigger('pageloaded', data);