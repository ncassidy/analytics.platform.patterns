(function () {

    var _helper = {
        getDefaultData: function (data) {
            var defaultData = {
                component: data.component,
                context: data.context,
            };

            return defaultData;
        }
    };


    $(window).on('HeroCarousel_click', function (event, data) {
        switch (data.component.target) {
            case 'bdyHero_Link':
                var trackingData = _helper.getDefaultData(data);
                trackingData.pev2 = 'Component Interaction: Hero | Title | Link';
                window.analytics.services.vendor.executeNavigationTracking(trackingData);
                break;
        }
    });    


    $(window).on('HeroCarousel_productsLoaded', function (event, data) {
        var trackingData = _helper.getDefaultData(data);
        trackingData.pev2 = 'Component Interaction: Hero | Products Loaded';
        window.analytics.services.vendor.executeNavigationTracking(trackingData);
    });

}());




// Trigger Example
var data = {
    component: {
        name: 'HeroCarousel',
        target: 'bdyHero_Link',
    },
    context: {
        LOB: 'Lodging'
    }
};
$(window).trigger('HeroCarousel_click', data);

// Results Loaded Example
var data = {
    component: {
        name: 'HeroCarousel',
        target: 'bdyHero',
    },
    context: {
        products: ['12345', '67890'],
        startDate: '12/1/2019',
        numberOfNights: 2,
        adultCount: 1,
        childCount: 2,
        LOB: 'Lodging'
    }
};
$(window).trigger('HeroCarousel_productsLoaded', data);