(function () {

    var config = {
        components: {
            HeroCarousel: {
                vendors: ['adobe']
            }
        },

        pages: {
            '/': {
                vendors: ['adobe', 'facebook']
            }
        }
    };

    // Define Site Config
    window.analytics = window.analytics || {};
    window.analytics.config = config;

}());