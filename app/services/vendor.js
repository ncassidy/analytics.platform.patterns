(function () {

    var validate = window.analytics.services.validate;


    var vendor = {
        executeNavigationTracking: function (data) {
            if (validate.isSet(data) && 
                validate.isSet(data.component) && 
                validate.isSet(data.component.name)) {
                var componentName = data.component.name;

                if (validate.isSet(window.analytics.config.components[componentName])) {
                    for (var x in window.analytics.config.components[componentName].vendors) {
                        var vendor = window.analytics.config.components[componentName].vendors[x];

                        if (validate.isSet(vendor) && 
                            window.analytics.config.components[componentName].vendors.includes(vendor) &&
                            validate.isSet(window.analytics.vendors[vendor].trackNavigation)) {
                            window.analytics.vendors[vendor].trackNavigation(data);
                        }
                    }
                }
            }
        },

        executePageloadTracking: function (data) {
            if (validate.isSet(data) && 
                validate.isSet(data.path)) {
                var pagePath = data.path;

                if (validate.isSet(window.analytics.config.pages[pagePath])) {
                    for (var x in window.analytics.config.pages[pagePath].vendors) {
                        var vendor = window.analytics.config.pages[pagePath].vendors[x];

                        if (validate.isSet(vendor) && 
                            window.analytics.config.pages[pagePath].vendors.includes(vendor) &&
                            validate.isSet(window.analytics.vendors[vendor].trackPageload)) {
                            window.analytics.vendors[vendor].trackPageload(data);
                        }
                    }
                }
            }
        }
    };

    
    // Define Vendor Service
    window.analytics = window.analytics || {};
    window.analytics.services = window.analytics.services || {};
    window.analytics.services.vendor = vendor;

}());