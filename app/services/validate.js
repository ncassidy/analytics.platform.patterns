(function () {

    var validate = {
        isSet: function (data) {
            return typeof data !== 'undefined' && data !== null && data !== '';
        }
    };

    
    // Define Vendor Service
    window.analytics = window.analytics || {};
    window.analytics.services = window.analytics.services || {};
    window.analytics.services.validate = validate;

}());