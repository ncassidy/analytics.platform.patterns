'use strict';

module.exports = {

    scripts: {
        // These scripts will be prepended to every build
        all: [],

        site: {
            origin: [
                //'app/libraries/*.js',
                'app/config.js',
                'app/services/*.js',
                'app/vendors/*.js',
                'app/components/*.js'
            ],
            debug: [],
            destination: 'dist/site'
        }
    },

    watch: {
        origin: [
            //'app/libraries/*.js',
            'app/config.js',
            'app/services/*.js',
            'app/vendors/*.js',
            'app/components/*.js'
        ]
    },

    jshint: {
        origin: [
            //'app/libraries/*.js',
            'app/config.js',
            'app/services/*.js',
            'app/vendors/*.js',
            'app/components/*.js'
        ]
    },

    server: {
        port: '8081'
    }
};
