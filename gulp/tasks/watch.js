'use strict';

var gulp = require('gulp');
var config = require('../configs/task-paths');
var sitesList = require('../configs/sites');

gulp.task('watch-scripts', function () {
    gulp.watch(config.watch.origin, { interval: 300 }, ['jshint-scripts', 'build-debug-scripts']);
});

// For those who want to use jslint in their editor
// and would like to only see other error messages
// in the console.
gulp.task('watch-scripts-no-jshint', ['get-commit-hash'], function () {
    gulp.watch(config.watch.origin, { interval: 300 }, ['build-debug-scripts']);
});

sitesList.forEach(function(resort) {
    // Debug for each site building a correct sourcemaps file.
    gulp.task('watch-scripts-' + resort + '-sourcemaps', ['get-commit-hash'], function () {
        gulp.watch(config.watch.origin, { interval: 300 }, [resort + '-debug-sourcemaps']);
    });
})
