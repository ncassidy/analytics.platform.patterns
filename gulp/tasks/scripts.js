'use strict';

var gulp = require('gulp');
var gulpUtil = require('gulp-util');
var sequence = require('gulp-sequence');
var filter = require('gulp-filter');
var config = require('../configs/task-paths');
var header = require('gulp-header');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var order = require('gulp-order');
var sourcemaps = require('gulp-sourcemaps');
var fs = require('fs');
var sitesList = require('../configs/sites');
var exec = require('child_process').exec;

var versionText = fs.readFileSync('VERSION', 'utf8').trim();
var commit;

var headerComment = function(buildType, filename) {
    var date = new Date();
    var buildDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + ' @ ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    var buildDateText = 'Analytics Platform Patterns Built on: ' + buildDate;
    var buildTypeText = "Build Type: " + buildType;
    var fileText = "Distribution File: " + filename;
    var commitHash = commit;

    var comment = "/*" + [buildDateText, versionText, fileText, buildTypeText, commitHash].join('\n') + "*/\n";

    return comment +
        "window.AnalyticsVersion = " + JSON.stringify(comment) + ";\n" +
        "console.log(window.AnalyticsVersion);\n";
};

var scriptBase = {
    base: './'
};
var uglifyFilter = ['**', '!**/loaderStart.js', '!**/loaderEnd.js'];

var uglifyFilterOpt = {
    restore: true
};

gulp.task('get-commit-hash', function (callback) {
    exec('git show --format="%h" --no-patch', function(err, stdout, stderr) {
        if (err) return;
        commit = stdout;
        callback();
    });
});

// Primary Tasks
gulp.task('build-debug-scripts', ['get-commit-hash'], function (callback) {
    var tasks = sitesList.map(function(site) {
        return site + '-debug';
    });

    sequence(tasks)(callback);
});

// These scripts are the same as production, but they include
// the debug depenendencies prepended so we can use requestly
// to serve them for testing
gulp.task('build-prod-test-scripts', ['get-commit-hash'], function (callback) {
    var tasks = sitesList.map(function(site) {
        return site + '-build-test';
    });

    sequence(tasks)(callback);
});

gulp.task('build-prod-scripts', ['get-commit-hash'], function (callback) {
    var tasks = sitesList.map(function(site) {
        return site + '-build';
    });

    sequence(tasks)(callback);
});


// Loop through all site creating
// the required tasks.
sitesList.forEach(function(site) {
    var unminifiedFile = site + '.js';
    var minifiedFile = site + '.min.js';

    // Debug (fast)
    gulp.task(site + '-debug', function () {
        var scripts = ['app/config/constants_dev.js'].concat(
            config.scripts[site].debug,
            config.scripts.all,
            config.scripts[site].origin
        );
        return debugWithSourcemaps(scripts, unminifiedFile, config.scripts[site].destination);
    });

    // Debug With Sourcemaps (slow)
    gulp.task(site + '-debug-sourcemaps', function () {
        var scripts = ['app/config/constants_dev.js'].concat(
            config.scripts[site].debug,
            config.scripts.all,
            config.scripts[site].origin
        );
        return debugWithSourcemaps(scripts, unminifiedFile, config.scripts[site].destination);
    });

    // Production
    gulp.task(site + '-build', function () {
        var scripts = ['app/config/constants_prod.js'].concat(
            config.scripts.all,
            config.scripts[site].origin
        );
        return buildWith(scripts, minifiedFile, config.scripts[site].destination);
    });

    // Production With Debug Assets Required for Ensighten Included
    gulp.task(site + '-build-test', function() {
        var scripts = ['app/config/constants_prod.js'].concat(
            config.scripts[site].debug,
            config.scripts.all,
            config.scripts[site].origin
        );
        return buildWith(scripts, unminifiedFile, config.scripts[site].destination);
    });
});



// Array String -> GulpStream
function requireFilesExist(files) {
    // Make sure non-glob files actually exist. If it doesn't
    // just return a noop stream. Otherwise return an error
    try {
        files.filter(function removeGlobs(f) {
            return f.indexOf('*') === -1;
        }).forEach(fs.statSync);
        return gulpUtil.noop();
    } catch(e) {
        throw new gulpUtil.PluginError(
            'scripts.js : requireFileExists', {
                message : e.message
            },
            { showStack : true }
        );
    }
}

// Create a production build
// scripts - Array of scripts to add to the build
// name - the file name to create
// destination - where to put the file
function buildWith(scripts, name, destination) {
    var f = filter(uglifyFilter, uglifyFilterOpt);
    var scripts = ['app/config/constants_prod.js'].concat(scripts);

    var scriptStream = gulp.src(scripts)
        .pipe(requireFilesExist(scripts))
        .pipe(f)

        // See here for uglify 2.0 options
        // https://github.com/mishoo/UglifyJS2#minify-options
        .pipe(uglify().on('error', gulpUtil.log))
        .pipe(f.restore)
        .pipe(order(scripts, scriptBase))
        .pipe(concat(name).on('error', gulpUtil.log))
        .pipe(header(headerComment('production', name)))
        .pipe(gulp.dest(destination));

    return scriptStream;
}

// Create a development build
// scripts - Array of scripts to add to the build
// name - the file name to create
// destination - where to put the file
function debugWith(scripts, name, destination) {
    var scriptStream = gulp.src(scripts)
        .pipe(requireFilesExist(scripts))
        .pipe(order(scripts, scriptBase))
        .pipe(concat(name).on('error', gulpUtil.log))
        .pipe(header(headerComment('debug', name)))
        .pipe(gulp.dest(destination));

    return scriptStream;
}

// Create a development build that generates a sourcemaps file
// scripts - Array of scripts to add to the build
// name - the file name to create
// destination - where to put the file
function debugWithSourcemaps(scripts, name, destination) {
    var scriptStream = gulp.src(scripts)
        .pipe(requireFilesExist(scripts))
        .pipe(sourcemaps.init())
        .pipe(order(scripts, scriptBase))
        .pipe(concat(name).on('error', gulpUtil.log))
        .pipe(header(headerComment('debug', name)))
        .pipe(sourcemaps.write("../../" + destination))
        .pipe(gulp.dest(destination));

    return scriptStream;
}
