'use strict';

var gulp = require('gulp');
var config = require('../configs/task-paths');
var jshint = require('gulp-jshint');
var stylishHint = require('jshint-stylish');


gulp.task('jshint-scripts', function () {
  var consoleStream = gulp.src(config.jshint.origin)
    .pipe(jshint({
      evil: true
    }))
    .pipe(jshint.reporter(stylishHint));

    return consoleStream;
});


gulp.task('jshint-scripts-report', function () {
  var reportStream = gulp.src(config.jshint.origin)
    .pipe(jshint())
    .pipe(jshint.reporter('gulp-jshint-html-reporter', {
      filename: __dirname + '/../../dist/jshint-report.html',
      createMissingFolders : true
    }));

  return reportStream;
});
