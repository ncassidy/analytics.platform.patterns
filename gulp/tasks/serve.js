'use strict';

var gulp = require('gulp');
var webserver = require('gulp-webserver');
var config = require('../configs/task-paths');

gulp.task('serve-scripts', function() {
    gulp.src('.')
        .pipe(webserver({
            port: config.server.port,
            https: true,
            livereload:       false,
            directoryListing: true
        }));
});
