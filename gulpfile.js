'use strict';

var gulp = require('gulp');
var sequence = require('gulp-sequence');
require('require-dir')('./gulp/tasks');

// CLI Interface Tasks
gulp.task('default', ['debug']);
gulp.task('dev-build', sequence(['build-debug-scripts']));

// Debug the minified build files. The only difference between prod-build-test and
// prod-build is it provides dependenices that are provided by ensighten.
gulp.task('prod-build-test', sequence(['build-prod-test-scripts']));

gulp.task('prod-build', sequence(['build-prod-scripts']));
gulp.task('watch', sequence(['build-debug-scripts', 'watch-scripts']));
gulp.task('server', sequence('serve-scripts'));
gulp.task('jshint', sequence(['jshint-scripts']));
gulp.task('jshint-report', sequence(['jshint-scripts-report']));
gulp.task('debug', sequence('build-debug-scripts', ['watch-scripts', 'serve-scripts']));
