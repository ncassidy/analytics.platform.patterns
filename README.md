# Analytics Platform Patterns Repository
The Analytics platform patterns repository serves the purpose of demonstrating the high level patterns for insuring a speration of concerns between a tracked application and the associated tracking code.

---

## Running a Development Environment

1. Install [node.js](https://nodejs.org/en/download/) (any version over 8.x.x)
2. Clone the analytics repository: `git clone git@bitbucket.org:ncassidy/analytics.platform.patterns.git`
3. Open a command line interface and navigate to the repository root: `/analytics.platform.patterns`.
4. Execute the following commands at the repository root.

```bash
npm install
npm run debug
```

This will start a server and automatically create build files for all sites. You can now proxy these files using the [Requestly](https://chrome.google.com/webstore/detail/requestly-redirect-url-mo/mdnleldcmiljblolnjhpnblkcekpdkpa?hl=en) plugin. Install the plugin and import the `requestly_rules.json` file into Requestly. Activate the rule for the site that you're working on, and you can now test locally built analytics code in a production or QA environment.

**Note: If you're working over HTTPS, and you're loading the page for the first time, you may need to open the proxied localhost file in it's own tab and manually accept the SSL certificate, e.g. (visit https://localhost:8081/dist/site/site.js in it's own tab). You should see the message "Your connection is not private" when viewing the proxied file.  Clicked Advanced and then "Proceed to localhost (unsafe)". After doing this, you should be able to proxy the analytics build into the target environment.**

---

## Debugging

Execute the following command at the repository root.

```bash
npm run debug
```

This command triggers an immediate dev-build, watches for analytics file changes, and runs a local server at: "https://localhost:8081" for file intercepting. When a file change is made, debug automatically executes a subsequent dev-build and serves the updated file. Type: `ctrl + c` at any time to exit debug's watch and server.

---

## Development and Production Builds

We use Gulp to concatenate and minify our files for deployment. Build files are put in the "dist/[site]" directories. The production version is minified while the development version is not.

For development builds, execute the following command in the repository's root. Build files will be at "dist/[site]/[site].js".

```bash
npm run dev-build
```

For production builds, executing the following command in the repository's root. Build files will be at "dist/[site]/[site].min.js".

```bash
npm run prod-build
```

---

## Code Quality

There are several tools that you can use to improve the quality of this repository's code. You can install your editor's [EditorConfig](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) plugin, which will automatically enforce whitespace removal and correct tabbing. You can also install your editor's [ESLint](https://eslint.org/) plugin, which will automatically check code against the rules in the `.eslintrc.json` file.